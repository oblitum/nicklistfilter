// clang++ -std=c++11 -O3 -shared -fPIC -Wall -Wextra -pedantic -o ~/.weechat/plugins/nicklistfilter.so nicklistfilter.cpp

#include <weechat/weechat-plugin.h>

WEECHAT_PLUGIN_NAME("nicklistfilter");
WEECHAT_PLUGIN_DESCRIPTION("nicklist filter plugin for WeeChat");
WEECHAT_PLUGIN_AUTHOR("Francisco Lopes <francisco@oblita.com>");
WEECHAT_PLUGIN_VERSION("0.1.0");
WEECHAT_PLUGIN_LICENSE("GPL3");

t_weechat_plugin *weechat_plugin = nullptr;

int nicklist_filter_cb(const void * /*pointer*/, void * /*data*/,
                       const char * /*signal*/, t_hashtable *hashtable) {
    auto buffer =
        static_cast<t_gui_buffer *>(weechat_hashtable_get(hashtable, "buffer"));
    auto nick =
        static_cast<t_gui_nick *>(weechat_hashtable_get(hashtable, "nick"));
    const char *nick_name =
        weechat_nicklist_nick_get_string(buffer, nick, "name");

    if (weechat_utf8_strlen(nick_name) > 15)
        weechat_nicklist_nick_set(buffer, nick, "visible", "0");

    return WEECHAT_RC_OK;
}

int weechat_plugin_init(t_weechat_plugin *plugin, int /*argc*/,
                        char * /*argv*/ []) {
    weechat_plugin = plugin;

    weechat_hook_hsignal("nicklist_nick_added", nicklist_filter_cb, nullptr,
                         nullptr);
    weechat_hook_hsignal("nicklist_nick_changed", nicklist_filter_cb, nullptr,
                         nullptr);

    return WEECHAT_RC_OK;
}

int weechat_plugin_end(t_weechat_plugin * /*plugin*/) { return WEECHAT_RC_OK; }
